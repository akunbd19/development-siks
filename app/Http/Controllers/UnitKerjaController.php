<?php

namespace App\Http\Controllers;

use App\Traits\ConsumeSrikandi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UnitKerjaController extends Controller
{
    use ConsumeSrikandi;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $response = $this->performRequest('GET', '/api/unit-kerja');

        return view('pages.administrasi.unit_kerja.index', [
            'data' => $response['data']
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        $response = $this->performRequest('GET', '/api/unit-kerja', $request->all());

        return response()->json(['data' => $response['data']]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $response = $this->performRequest('GET', '/api/unit-kerja');

        return view('pages.administrasi.unit_kerja.create', [
            'organisasi' => $response['data'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'parent_id' => 'required',
            'nama' => 'required',
        ];

        $messages = [
            'parent_id.required' => 'Data Induk harus dipilih',
            'nama.required' => 'Nama harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->performRequest('POST', '/api/unit-kerja/store', $request->all());

        return redirect()->route('unit-kerja.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->performRequest('GET', '/api/unit-kerja/' . $id . '/show');

        if ($item['success'] != 200)
            return redirect()->route('unit-kerja.index');

        $organisasi = $this->performRequest('GET', '/api/unit-kerja');

        return view('pages.administrasi.unit_kerja.edit', [
            'organisasi' => $organisasi['data'],
            'data' => $item['data'],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'parent_id' => 'required',
            'nama' => 'required',
        ];

        $messages = [
            'parent_id.required' => 'Data Induk harus dipilih',
            'nama.required' => 'Nama harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->performRequest('POST', '/api/unit-kerja/update/' . $id, $request->all());

        return redirect()->route('unit-kerja.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $id = request('id');
        $response = $this->performRequest('POST', '/api/unit-kerja/delete/' . $id);

        return response()->json([
            'status' => true,
            'message' => $response['message']
        ]);
    }
}
