<?php

namespace App\Http\Controllers\Api;

use App\Services\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $auth = Auth::attempt($request->username, $request->password);
        $message = isset($auth['message']) ? $auth['message'] : '';
        $message = gettype($message) == 'array' ? $message[0] : $message;

        if (isset($auth['success']) && $auth['success']) {
            return redirect()->route('dashboard.index');
        }


        return redirect()->back()->with('alert-' . $auth['type'], $message);
    }
}
