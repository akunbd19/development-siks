<?php

namespace App\Http\Controllers;

use App\Traits\ConsumeSrikandi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PenggunaController extends Controller
{
    use ConsumeSrikandi;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $response = $this->performRequest('GET', '/api/user');

        return view('pages.administrasi.pengguna.index', [
            'data' => $response['data']
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        $response = $this->performRequest('GET', '/api/user', $request->all());

        return response()->json(['data' => $response['data']]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $response = $this->performRequest('GET', '/api/unit-kerja');
        $jabatan = $this->performRequest('GET', '/api/unit-kerja/jabatan');

        return view('pages.administrasi.pengguna.create', [
            'organisasi' => $response['data'],
            'jabatan'=> $jabatan['data'],
        ]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'organisasi_id' => 'required',
            'jabatan_id' => 'required',
            'grup_jabatan_id' => 'required',
            'tipe_pengguna_id' => 'required',
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
            'email' => 'required',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'nik' => 'required',
            'nomor_induk' => 'required',
            'telepon' => 'required',
            'foto' => 'required',
            'roles' => 'required',
        ];

        $messages = [
            'organisasi_id.required' => 'Nomor Induk Pegawai Harus Diisi',
            'jabatan_id.required' => 'Jabatan Harus Pilih',
            'grup_jabatan_id.required' => 'Grup Jabatan harus Dipilih',
            'tipe_pengguna_id.required' => 'Tipe Pengguna Harus Dipilih',
            'nama.required' => 'Nama Harus Diisi',
            'username.required' => 'Username Harus Diisi',
            'password.required' => 'Kata Sandi Harus Diisi',
            'nik.required' => 'Nomor NIK Harus Diisi',
            'nomor_induk.required' => 'Nomor Induk Harus Diisi',
            'telepon.required' => 'Harus Memasukkan Nomor Telepon',
            'foto.required' => 'Harus Mengunggah Foto',
            'roles.required' => 'Role Harus Dipilih',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->performRequest('POST', '/api/user/store', $request->all());

        return redirect()->route('pengguna.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->performRequest('GET', '/api/user/' . $id . '/show');

        if ($item['success'] != 200)
            return redirect()->route('pengguna.index');

        $organisasi = $this->performRequest('GET', '/api/user');

        return view('pages.administrasi.pengguna.edit', [
            'organisasi' => $organisasi['data'],
            'data' => $item['data'],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'organisasi_id' => 'required',
            'jabatan_id' => 'required',
            'grup_jabatan_id' => 'required',
            'tipe_pengguna_id' => 'required',
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
            'nik' => 'required',
            'nomor_induk' => 'required',
            'telepon' => 'required',
            'foto' => 'required',
            'roles' => 'required',
        ];

        $messages = [
            'organisasi_id.required' => 'Nomor Induk Pegawai Harus Diisi',
            'jabatan_id.required' => 'Jabatan Harus Pilih',
            'grup_jabatan_id.required' => 'Grup Jabatan harus Dipilih',
            'tipe_pengguna_id.required' => 'Tipe Pengguna Harus Dipilih',
            'nama.required' => 'Nama Harus Diisi',
            'username.required' => 'Username Harus Diisi',
            'password.required' => 'Kata Sandi Harus Diisi',
            'nik.required' => 'Nomor NIK Harus Diisi',
            'nomor_induk.required' => 'Nomor Induk Harus Diisi',
            'telepon.required' => 'Harus Memasukkan Nomor Telepon',
            'foto.required' => 'Harus Mengunggah Foto',
            'role.required' => 'Role Harus Dipilih',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->performRequest('POST', '/api/user/update/' . $id, $request->all());

        return redirect()->route('pengguna.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $id = request('id');
        $response = $this->performRequest('POST', '/api/user/delete/' . $id);

        return response()->json([
            'status' => true,
            'message' => $response['message']
        ]);
    }
}
