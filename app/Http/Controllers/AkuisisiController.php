<?php

namespace App\Http\Controllers;

use App\Constants\StatusAkuisisi;
use App\Lembaga;
use App\Model\Akuisisi;
use App\Model\AkuisisiBeritaAcara;
use App\Model\AkuisisiTimPenilai;
use App\Services\Auth;
use App\Traits\ConsumeSrikandi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use NcJoes\OfficeConverter\OfficeConverter;
use PhpOffice\PhpWord\TemplateProcessor;

class AkuisisiController extends Controller
{
    use ConsumeSrikandi;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /*$response = $this->performRequest('GET', '/api/berkas');

        $data = [];
        foreach ($response['data'] as $key => $item) {
            $data[] = $item;
        }*/

        return view('pages.akuisisi.akuisisi.index');
    }

    public function getList(Request $request)
    {
        $http_query = http_build_query($request->all());
        $response = $this->performRequest('GET', '/api/berkas' . ($http_query ? '?' . $http_query : ''));

        $data = [];
        foreach ($response['data'] as $key => $item) {
            $items = AkuisisiTimPenilai::query()->where('berkas_id', $item->id)->get();
            $item->team = $items;

            $data[] = $item;
        }

        return response()->json(compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(Request $request)
    {
        $rules = [
            'ketua' => 'required',
            'anggota' => 'required',
        ];

        $ruleMessages = [
            'ketua.required' => 'ketua harus diisi',
            'anggota.*.required' => 'anggota wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $htmlErrors = '<ul>';

            foreach ($errors as $item) {
                $htmlErrors .= '<li>' . $item . '</li>';
            }

            $htmlErrors .= '</ul>';

            return response()->json([
                'code' => Response::HTTP_OK,
                'status' => false,
                'error' => $validator->errors()->all(),
                'message' => '<div class="alert alert-danger">Mohon masukkan data yang sesuai dengan formulir:<br>' . $htmlErrors . '</div>'
            ]);
        }

        DB::beginTransaction();

        try {
            $data_insert = [];
            foreach ($request->anggota as $key => $item) {
                if ($key == 0)
                    $data_insert[] = [
                        'berkas_id' => $request->berkas_id,
                        'user_id' => $request->ketua,
                        'role' => 'ketua',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];

                $data_insert[] = [
                    'berkas_id' => $request->berkas_id,
                    'user_id' => $item,
                    'role' => 'anggota',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            AkuisisiTimPenilai::query()->insert($data_insert);

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_OK,
                'status' => true,
                'message' => '<div class="alert alert-success">Data berhasil disimpan</div>'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'status' => false,
                'message' => '<div class="alert alert-danger">Data gagal disimpan</div>'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function confirm(Request $request)
    {
        $rules = [
            'kode_klasifikasi' => 'required',
            'berkas' => 'required|array',
            'penerima' => 'required',
        ];

        $ruleMessages = [
            'kode_klasifikasi.required' => 'kode klasifikasi harus diisi',
            'berkas.required' => 'berkas wajib diisi',
            'penerima.required' => 'penerima harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $htmlErrors = '<ul>';

            foreach ($errors as $item) {
                $htmlErrors .= '<li>' . $item . '</li>';
            }

            $htmlErrors .= '</ul>';

            return response()->json([
                'code' => Response::HTTP_OK,
                'status' => false,
                'error' => $validator->errors()->all(),
                'message' => '<div class="alert alert-danger">Mohon masukkan data yang sesuai dengan formulir:<br>' . $htmlErrors . '</div>'
            ]);
        }

        DB::beginTransaction();

        try {
            $count_ba = AkuisisiBeritaAcara::query()->count();
            $ba_code = $request->kode_klasifikasi . '/' . sprintf("%03d", ($count_ba + 1)) . '/' . Carbon::now()->format('Y');

            $template_ba = "public/templates/berita-acara.docx";

            $file_directory = storage_path() . '/app/public/berita-acara/';
            if (!file_exists($file_directory))
                mkdir($file_directory, 0777);

            $file_name = time() . '' . sprintf("%05d", random_int(0, 1000));
            $file_name_docx = $file_name . '.docx';
            $file_name_pdf = $file_name . '.pdf';

            $template = new TemplateProcessor(storage_path('app/' . $template_ba));
            $template->setValue('nomor_berita_acara', $ba_code);
            $template->saveAs($file_directory . $file_name_docx);
            $exec = 'et HOME=/tmp && soffice --headless --convert-to pdf "C:\xampp\htdocs\siks\storage\app\public\berita-acara\163654053700169.docx" --outdir "C:\xampp\htdocs\siks\storage\app\public\berita-acara';
            dd();

            $converter = new OfficeConverter($file_directory . $file_name_docx, $file_directory, 'soffice');
            $converter->convertTo($file_name_pdf);

            Storage::disk('local')->delete('public/berita-acara/' . $file_name_docx);

            $ba = new AkuisisiBeritaAcara();
            $ba->kode_klasifikasi = $request->kode_klasifikasi;
            $ba->penerima_id = $request->penerima;
            $ba->nama_penerima = $request->penerima;
            $ba->nomor = $ba_code;
            $ba->file_name = $file_name_pdf;
            $ba->file_path = 'app/public/berita-acara/' . $file_name_pdf;
            $ba->created_by = Auth::user()->username;
            $ba->save();

            $data_insert_head = [];
            foreach ($request->berkas as $key => $item) {
                $data_insert_head[] = [
                    'kode_klasifikasi' => $request->kode_klasifikasi,
                    'berkas_id' => $item['berkas_id'],
                    'berita_acara_id' => $ba->id,
                    'penerima_id' => $request->penerima,
                    'nama_penerima' => $request->penerima,
                    'nama_berkas' => $item['nama_berkas'],
                    'kurun' => $item['kurun'],
                    'status' => StatusAkuisisi::SELESAI_AKUISISI,
                    'sifat' => $item['sifat'],
                    'status_arsip' => $item['status_arsip'],
                    'data' => $item['data'],
                    'created_by' => Auth::user()->username,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            Akuisisi::query()->insert($data_insert_head);

            /* todo: save item akuisisi */

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_OK,
                'status' => true,
                'message' => '<div class="alert alert-success">Data berhasil disimpan</div>',
                'data' => [
                    'bapp_code' => $ba_code
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'status' => false,
                'message' => '<div class="alert alert-danger">Data gagal disimpan</div>'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = (object)[
            'id' => $id,
            'kode_klasifikasi' => 'LK.02',
            'desc' => 'Berkas Pengajuan Dupak a.n Meri Andriani',
            'penyusutan_akhir' => 'Permanen',
            'lokasi' => 'Gedung ANRI',
            'tahun' => 2023,
            'jumlah' => 2,
            'jra_aktif' => Carbon::now()->addMonth(11)->addYear(1)->format('d M Y'),
        ];

        $berkas = [
            (object)[
                'id' => rand(),
                'kode_klasifikasi' => 'Masuk',
                'desc' => 'D1.01.02/1/2021',
                'jra_aktif' => Carbon::now()->subMonth(2)->format('d M Y'),
                'tahun' => 'File Pendukung Dupak',
                'team' => false,
            ],
            (object)[
                'id' => rand(),
                'kode_klasifikasi' => 'Masuk',
                'desc' => 'D1.01.02/1/2021',
                'jra_aktif' => Carbon::now()->subMonth(121)->format('d M Y'),
                'tahun' => 'File Pendukung Dupak',
                'team' => true,
            ],
        ];

        return view('pages.akuisisi.akuisisi.show', compact(['data', 'berkas']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
