<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use App\Traits\ConsumeSrikandi;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Exception\GuzzleException;

class Auth
{
    static function attempt($username, $password)
    {
        try {
            $client = new Client([
                'verify' => false
            ]);

            $response = $client->request('POST', config('srikandi.url') . '/api/login', [
                'json' => [
                    'username' => $username,
                    'password' => $password,
                ]
            ]);

            if ($response->getStatusCode() != Response::HTTP_OK)
                return [
                    'type' => 'danger',
                    'success' => false,
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message' => 'Terjadi kesalahan pada server.',
                ];

            $result = $response->getBody()->getContents();
            $result = json_decode($result);

            if (!$result)
                return [
                    'type' => 'danger',
                    'success' => false,
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message' => 'Terjadi kesalahan pada server.',
                ];

            if ($result->code != 200)
                return [
                    'type' => 'danger',
                    'success' => false,
                    'code' => $result->code,
                    'message' => $result->message,
                ];

            $session_id = Str::uuid()->toString() . '-' . rand();

            $token = $result->token;
            $user = $result->user_session;
            $user->role = count($user->roles) > 0 ? $user->roles[0]->name : '-';

            if ($user->status != 'AKTIF') {
                return [
                    'type' => 'warning',
                    'success' => false,
                    'code' => Response::HTTP_FORBIDDEN,
                    'message' => 'Akun tidak aktif, harap hubungi administrator.',
                ];
            }

            /*
             * set session key user to browser
             * */
            session()->put('me', $session_id);

            Redis::set('token_' . $session_id, $token);
            Redis::set('user_' . $session_id, json_encode($user));

            return [
                'type' => 'info',
                'success' => true,
                'code' => Response::HTTP_OK,
                'message' => $result->message ? $result->message : '',
            ];
        } catch (GuzzleException $e) {
            return [
                'type' => 'danger',
                'success' => false,
                'code' => $e->getCode() == 0 ? Response::HTTP_INTERNAL_SERVER_ERROR : $e->getCode(),
                'message' => 'Terjadi kesalahan pada server.',
            ];
        }
    }

    static function user()
    {
        $session_key = session('me');
        $user = Redis::get('user_' . $session_key);

        return json_decode($user);
    }

    static function check()
    {
        $session_key = session('me');
        $token = Redis::get('token_' . $session_key);
        $explode = explode('.', $token);

        if (count($explode) <= 1 || empty($token)) {
            self::logout();
            return false;
        }

        $payload_data = json_decode(base64_decode($explode[1]));

        return Carbon::now()->timestamp < $payload_data->exp;
    }

    static function logout()
    {
        $session_key = session('me');
        session()->flush();

        Redis::del('token_' . $session_key);
        Redis::del('user_' . $session_key);
    }

    static function token()
    {
        $session_key = session('me');
        $token = Redis::get('token_' . $session_key);

        return $token;
    }
}
