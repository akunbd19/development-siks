<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AkuisisiTimPenilai extends Model
{
    protected $table = 'akuisisi_tim_penilai';

    protected $fillable = [
        'berkas_id',
        'user_id',
        'role',
    ];
}
