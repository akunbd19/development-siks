<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AkuisisiBeritaAcara extends Model
{
    protected $table = 'akuisisi_berita_acara';

    protected $fillable = [
        'kode_klasifikasi',
        'penerima_id',
        'nama_penerima',
        'nomor',
        'file_name',
        'file_path',
        'created_by',
    ];
}
