<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\BAAkuisisiController;
use Illuminate\Support\Facades\Route;

Route::post('/signin', 'Api\AuthController@login')->name('signin');

Route::group(['middleware' => ['authenticated']], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard.index');
    });

    Route::group(['prefix' => 'daftar-arsip'], function () {
        Route::get('/', 'ArsipController@index')->name('arsip.index');
        Route::post('/store', 'ArsipController@store')->name('arsip.store');
        Route::get('/{id}/show', 'ArsipController@show')->name('arsip.show');
        Route::post('/update/{id}', 'ArsipController@update')->name('arsip.update');
        Route::post('/destroy/{id}', 'ArsipController@destroy')->name('arsip.destroy');
    });

    Route::group(['prefix' => 'audit-trial'], function () {
        Route::get('/', 'AuditTrialController@index')->name('audit-trial.index');
    });

    Route::group(['prefix' => 'group-hak-akses'], function () {
        Route::get('/', 'GroupHakAksesController@index')->name('group-hak-akses.index');
    });

    Route::group(['prefix' => 'group-jabatan'], function () {
        Route::get('/', 'GroupJabatanController@index')->name('group-jabatan.index');
    });

    Route::group(['prefix' => 'instansi'], function () {
        Route::get('/', 'InstansiController@index')->name('instansi.index');
    });

    Route::group(['prefix' => 'jabatan'], function () {
        Route::get('/', 'JabatanController@index')->name('jabatan.index');
        Route::get('/get', 'JabatanController@get')->name('jabatan.get');
        Route::get('/create', 'JabatanController@create')->name('jabatan.create');
        Route::post('/store', 'JabatanController@store')->name('jabatan.store');
        Route::get('/{id}/edit', 'JabatanController@edit')->name('jabatan.edit');
        Route::post('/update/{id}', 'JabatanController@update')->name('jabatan.update');
        Route::post('/delete', 'JabatanController@destroy')->name('jabatan.destroy');
        });



    Route::group(['prefix' => 'jenis-organisasi'], function () {
        Route::get('/', 'JenisOrganisasiController@index')->name('jenis-organisasi.index');

    });

    Route::group(['prefix' => 'pengguna'], function () {
        Route::get('/', 'PenggunaController@index')->name('pengguna.index');
        Route::get('/get', 'PenggunaController@get')->name('pengguna.get');
        Route::get('/create', 'PenggunaController@create')->name('pengguna.create');
        Route::post('/store', 'PenggunaController@store')->name('pengguna.store');
        Route::get('/{id}/edit', 'PenggunaController@edit')->name('pengguna.edit');
        Route::post('/update/{id}', 'PenggunaController@update')->name('pengguna.update');
        Route::post('/delete', 'JabatanController@destroy')->name('pengguna.destroy');
    });

    Route::group(['prefix' => 'referensi-instansi'], function () {
        Route::get('/', 'ReferensiInstansiController@index')->name('referensiinstansi.index');
    });

    Route::group(['prefix' => 'unit-kerja'], function () {
        Route::get('/', 'UnitKerjaController@index')->name('unit-kerja.index');
        Route::get('/get', 'UnitKerjaController@get')->name('unit-kerja.get');
        Route::get('/create', 'UnitKerjaController@create')->name('unit-kerja.create');
        Route::post('/store', 'UnitKerjaController@store')->name('unit-kerja.store');
        Route::get('/{id}/edit', 'UnitKerjaController@edit')->name('unit-kerja.edit');
        Route::post('/update/{id}', 'UnitKerjaController@update')->name('unit-kerja.update');
        Route::post('/delete', 'UnitKerjaController@destroy')->name('unit-kerja.destroy');
    });

//    akuisisi
    Route::prefix('akuisisi')->name('akuisisi.')->group(function () {
        Route::prefix('usul-serah')->name('usul-serah.')->group(function () {
            Route::get('/', 'UsulSerahController@index')->name('index');
            Route::get('/get', 'UsulSerahController@get')->name('get');
            Route::get('/create', 'UsulSerahController@create')->name('create');
            Route::get('/{id}', 'UsulSerahController@show')->name('show');
            Route::post('/store', 'UsulSerahController@store')->name('store');
            Route::post('/update/{id}', 'UsulSerahController@update')->name('update');
            Route::post('/delete', 'UsulSerahController@destroy')->name('destroy');
        });
        Route::prefix('ba-akuisisi')->name('ba-akuisisi.')->group(function () {
            Route::get('/', 'BAAkuisisiController@index')->name('index');
            Route::get('/get', 'BAAkuisisiController@get')->name('get');
            Route::get('/create', 'BAAkuisisiController@create')->name('create');
            Route::get('/{id}', 'BAAkuisisiController@show')->name('show');
            Route::get('/detail/{id}', 'BAAkuisisiController@detail')->name('detail');
            Route::post('/store', 'BAAkuisisiController@store')->name('store');
            Route::post('/update/{id}', 'BAAkuisisiController@update')->name('update');
            Route::post('/delete', 'BAAkuisisiController@destroy')->name('destroy');
        });
        Route::group([], function () {
            Route::get('/', 'AkuisisiController@index')->name('index');
            Route::get('/get', 'AkuisisiController@getList')->name('get');
            Route::get('/create', 'AkuisisiController@create')->name('create');
            Route::get('/{id}', 'AkuisisiController@show')->name('show');
            Route::post('/store', 'AkuisisiController@store')->name('store');
            Route::post('/confirm', 'AkuisisiController@confirm')->name('confirm');
            Route::post('/update/{id}', 'AkuisisiController@update')->name('update');
            Route::post('/delete', 'AkuisisiController@destroy')->name('destroy');
        });
        
    });

//    pengolahan
    Route::prefix('pengolahan')->name('pengolahan.')->group(function () {
        Route::prefix('arsip-statis')->name('arsip-statis.')->group(function () {
            Route::get('/', 'ArsipStatisController@index')->name('index');
            Route::get('/get', 'ArsipStatisController@get')->name('get');
            Route::get('/create', 'ArsipStatisController@create')->name('create');
            Route::get('/{id}', 'ArsipStatisController@show')->name('show');
            Route::post('/store', 'ArsipStatisController@store')->name('store');
            Route::post('/update/{id}', 'ArsipStatisController@update')->name('update');
            Route::post('/delete', 'ArsipStatisController@destroy')->name('destroy');
        });
        Route::group([], function () {
            Route::get('/', 'AkuisisiController@index')->name('index');
            Route::get('/get', 'AkuisisiController@getList')->name('get');
            Route::get('/create', 'AkuisisiController@create')->name('create');
            Route::get('/{id}', 'AkuisisiController@show')->name('show');
            Route::post('/store', 'AkuisisiController@store')->name('store');
            Route::post('/confirm', 'AkuisisiController@confirm')->name('confirm');
            Route::post('/update/{id}', 'AkuisisiController@update')->name('update');
            Route::post('/delete', 'AkuisisiController@destroy')->name('destroy');
        });
    });
});

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
