<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkuisisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akuisisi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_klasifikasi');
            $table->bigInteger('berkas_id');
            $table->bigInteger('berita_acara_id')->nullable()->index();
            $table->string('penerima_id');
            $table->string('nama_penerima');
            $table->string('nama_berkas');
            $table->string('kurun');
            $table->string('status');
            $table->string('sifat');
            $table->string('status_arsip');
            $table->longText('data');
            $table->string('created_by');
            $table->timestamps();

            $table->foreign('berita_acara_id')->references('id')->on('akuisisi_berita_acara')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akuisisi');
    }
}
