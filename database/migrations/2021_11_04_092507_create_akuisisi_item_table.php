<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkuisisiItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akuisisi_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('berkas_id');
            $table->bigInteger('item_id');
            $table->string('tipe_naskah');
            $table->string('jenis_naskah');
            $table->string('nomor');
            $table->string('hal');
            $table->string('file');
            $table->string('file_url');
            $table->timestamp('tanggal');
            $table->text('catatan')->nullable();
            $table->integer('tindak_lanjut')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akuisisi_item');
    }
}
