<div id="modal-development" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning alert-styled-left alert-dismissible">
                    <span class="font-weight-semibold">Hai, {{ Auth::user()->nama_lengkap }}</span> bagian ini sedang
                    tahap <i><b>development</b></i> dimohon kesabarannya. Terima kasih
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-block btn-outline-warning" data-dismiss="modal">
                    <b><i class="icon-cross2 mr-2"></i></b> Tutup
                </button>
            </div>
        </div>
    </div>
</div>
