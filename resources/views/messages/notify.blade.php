<script type="text/javascript">
    $(document).ready(function () {
        var success = '{{ Session::has('success') }}';
        var info = '{{ Session::has('info') }}';
        var error = '{{ Session::has('error') }}';
        var warning = '{{ Session::has('warning') }}';

        if (success) {
            new Noty({
                theme: ' alert alert-success alert-styled-left p-0',
                text: '{{ Session::get('success') }}',
                type: 'success',
                progressBar: false,
                timeout: 2500
            }).show();
        } else if (info) {
            new Noty({
                theme: ' alert alert-info alert-styled-left p-0',
                text: '{{ Session::get('info') }}',
                type: 'info',
                progressBar: false,
                timeout: 2500
            }).show();
        } else if (error) {
            new Noty({
                theme: ' alert alert-danger alert-styled-left p-0',
                text: '{{ Session::get('error') }}',
                type: 'error',
                progressBar: false,
                timeout: 2500
            }).show();
        } else if (warning) {
            new Noty({
                theme: ' alert alert-warning alert-styled-left p-0',
                text: '{{ Session::get('warning') }}',
                type: 'warning',
                progressBar: false,
                timeout: 2500
            }).show();
        }
    });
</script>
