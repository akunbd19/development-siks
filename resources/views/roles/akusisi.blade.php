<li class="nav-item nav-item-submenu @yield('open_akuisisi')">
    <a href="#" class="nav-link @yield('akuisisi')">
        <i class="icon-drawer-in"></i> <span>Akuisisi</span>
    </a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{ route('akuisisi.usul-serah.index') }}" class="nav-link @yield('akuisisi_usul-serah_index')">
                <span>
                    Arsip Usul Serah
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('akuisisi.index') }}" class="nav-link @yield('akuisisi_index')">
                <span>
                    Akuisisi Arsip
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('akuisisi.ba-akuisisi.index') }}" class="nav-link @yield('akuisisi_ba-akuisisi_index')">
                <span>
                    BA Akuisisi
                </span>
            </a>
        </li>
    </ul>
</li>
