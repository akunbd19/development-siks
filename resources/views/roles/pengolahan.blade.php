<li class="nav-item nav-item-submenu @yield('open_pengolahan')">
    <a href="#" class="nav-link @yield('pengolahan')">
        <i class="icon-stack"></i> <span>Pengolahan</span>
    </a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{ route('pengolahan.arsip-statis.index') }}" class="nav-link @yield('pengolahan_arsip-statis_index')">
                <span>
                    Daftar Arsip Statis
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('pengolahan.index') }}" class="nav-link @yield('pengolahan_index')">
                <span>
                    Inventaris
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('pengolahan.arsip-statis.index') }}"
               class="nav-link @yield('pengolahan.ba-pengolahan.index')">
                <span>
                    Guide
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('pengolahan.arsip-statis.index') }}"
               class="nav-link @yield('pengolahan.ba-pengolahan.index')">
                <span>
                    Naskah Sumber
                </span>
            </a>
        </li>
    </ul>
</li>
