@extends('layouts.begin_back')

@section('dashboard', 'active')

@section('title', 'Dashboard')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>
                    <i class="icon-home4 mr-2"></i>
                    <span class="font-weight-semibold">Beranda</span> -
                    Selamat datang, {{\App\Services\Auth::user()->nama}}!
                </h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">
                <div class="row">

                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
@endsection
