<div class="modal fade" id="modal-team" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">
                    Tambah Tim Penilai
                </h4>
            </div>
            <form action="{{ route('akuisisi.store') }}" method="POST" id="form-create-team">
                @csrf
                <div class="modal-body">
                    <div class="message"></div>
                    <div class="form-row">
                        <input type="hidden" name="berkas_id" value="" id="berkas_id">
                        <div class="form-group col-12">
                            <label for="ketua" class="d-block">
                                Ketua <span class="text-danger">*</span>
                            </label>
                            <select name="ketua"
                                    id="ketua"
                                    class="form-control form-control-select2"
                                    data-placeholder="Pilih Ketua Tim Penilai"
                                    required>
                                <option value="1">Ketua 1</option>
                                <option value="2">Ketua 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <h4>Anggota</h4>
                        </div>
                        <div class="col-4 text-right">
                            <button type="button"
                                    class="btn btn-outline-primary btn-sm btn-add-anggota"
                                    title="Tambah Anggota">
                                <i class="icon-plus2"></i>
                            </button>
                        </div>

                        <div class="col-12">
                            <table class="table" id="table-anggota">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Anggota</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--<tr>
                                    <td>1</td>
                                    <td style="width: 80%;" class="pr-0">
                                        <div class="form-row">
                                            <div class="form-group col-12 my-0">
                                                <select name="anggota[]"
                                                        id="anggota2"
                                                        class="form-control form-control-select2"
                                                        data-placeholder="Pilih Anggota"
                                                        required>
                                                    <option value="1">Anggota 1</option>
                                                    <option value="2">Anggota 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-outline-danger btn-sm"
                                                title="Hapus Anggota">
                                            <i class="icon-trash"></i>
                                        </button>
                                    </td>
                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" id="btn-cancel-save"
                            data-dismiss="modal">
                        Batal
                    </button>
                    <button type="submit" class="btn btn-primary" id="btn-save">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
