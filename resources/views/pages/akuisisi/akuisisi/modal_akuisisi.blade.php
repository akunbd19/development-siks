<div class="modal fade" id="modal-akuisisi" aria-labelledby="modalLabelAkuisisi" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelAkuisisi">
                    Konfirmasi
                </h4>
            </div>
            <form action="{{ route('akuisisi.confirm') }}" method="POST" id="form-create-akuisisi">
                @csrf
                <div class="modal-body">
                    <div class="message"></div>

                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm" id="table-akuisisi">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="penerima" class="d-block">
                                        Penerima <span class="text-danger">*</span>
                                    </label>
                                    <select name="penerima"
                                            id="penerima"
                                            class="form-control form-control-select2"
                                            data-placeholder="Pilih Penerima"
                                            required>
                                        <option value="1">Ketua 1</option>
                                        <option value="2">Ketua 2</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="kode_klasifikasi" class="d-block">
                                        Klasifikasi <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" name="kode_klasifikasi" id="kode_klasifikasi" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" id="btn-cancel-save-akuisisi"
                            data-dismiss="modal">
                        Batal
                    </button>
                    <button type="submit" class="btn btn-success" id="btn-save-akuisisi">Akuisisi</button>
                </div>
            </form>
        </div>
    </div>
</div>
