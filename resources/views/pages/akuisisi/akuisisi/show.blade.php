@extends('layouts.begin_back')

@section('open_akuisisi', 'nav-item-expanded nav-item-open')

@section('akuisisi', 'active')

@section('akuisisi_index', 'active')

@section('title', 'Akuisisi Arsip Detail')

@section('content')
    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="icon-folder-open2 mr-2"></i> <span class="font-weight-semibold">Akuisisi Arsip</span> -
                Detail
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('akuisisi.index') }}" class="btn bg-grey btn-labeled btn-labeled-left">
                    <b><i class="icon-arrow-left8"></i></b> Kembali
                </a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Detail Akuisisi Arsip </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="row">
                                            <div class="col-4"><strong>Kode Klasifikasi</strong></div>
                                            <div class="col-8">
                                                : <code>{{$data->kode_klasifikasi}}</code>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <strong>Informasi Uraian Arsip</strong>
                                            </div>
                                            <div class="col-8">: {{$data->desc}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <strong>Penyusutan Akhir</strong>
                                            </div>
                                            <div class="col-8">: {{$data->penyusutan_akhir}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <strong>Akhir Retensi Aktif</strong>
                                            </div>
                                            <div class="col-8">: {{$data->jra_aktif}}</div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <strong>Tanggal dibuat</strong>
                                                    </div>
                                                    <div class="col-8">: {{$data->jra_aktif}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <strong>Lokasi Fisik</strong>
                                                    </div>
                                                    <div class="col-8">: {{$data->lokasi}}</div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <strong>Longitude</strong>
                                                    </div>
                                                    <div class="col-8">: {{$data->jra_aktif}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <strong>Latitude</strong>
                                                    </div>
                                                    <div class="col-8">: {{$data->lokasi}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Daftar Isi Berkas</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table datatable-responsive-control-right table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width: 20%">Jenis Naskah</th>
                                        <th style="width: 20%">Nomor Naskah</th>
                                        <th>Tanggal</th>
                                        <th>Hal</th>
                                        <th style="width: 5%;" class="text-center">File</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($berkas as $key => $item)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $item->kode_klasifikasi }}</td>
                                            <td>{{ $item->desc }}</td>
                                            <td>{{ $item->jra_aktif }}</td>
                                            <td>{{ $item->tahun }}</td>
                                            <td style="white-space: nowrap;">
                                                <a href="#"
                                                   target="_blank"
                                                   class="list-icons-item text-danger btn-icon">
                                                    <i class="icon-file-download"></i>
                                                </a>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endpush

@section('footer-script')
    <script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>
        $(document).ready(function () {
            initDatatable();

            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                dropdownAutoWidth: true,
                width: 'auto'
            });
        });

        function initDatatable() {
            $('.datatable-responsive-control-right').DataTable();
        }
    </script>
@endsection
