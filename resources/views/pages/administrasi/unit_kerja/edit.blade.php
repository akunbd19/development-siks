@extends('layouts.begin_back')

@section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('organisasi', 'active')

@section('title', 'Edit Unit Kerja / Satker')

@section('content')
    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="icon-city mr-2"></i> <span class="font-weight-semibold">Unit Kerja / Satker</span> - Edit
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('unit-kerja.index') }}" class="btn bg-grey btn-labeled btn-labeled-left"><b><i
                            class="icon-arrow-left8"></i></b>Kembali</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Form Edit Unit Kerja / Satker </h5>
                            </div>
                            <form action="{{ route('unit-kerja.update', $data->id) }}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Induknya <span class="text-danger">*</span></label>
                                                <select name="parent_id" id="parent_id" class="form-control select2"
                                                        data-placeholder="Pilih Organisasi"
                                                    {{$data->id == \App\Services\Auth::user()->organisasi->public_id ? 'disabled' : ''}}>
                                                    <option></option>
                                                    @foreach($organisasi as $val)
                                                        <option value="{{$val->id}}"
                                                            {{$val->id == $data->parent_id?'selected' : ''}}>{{$val->nama}}
                                                        </option>
                                                    @endforeach
                                                    @if($errors->has('parent_id'))
                                                        <label id="basic-error"
                                                               class="validation-invalid-label"
                                                               for="basic">
                                                            {{$errors->first('parent_id')}}
                                                        </label>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama" id="nama" class="form-control"
                                                       value="{{$data->nama}}"
                                                    {{$data->id == \App\Services\Auth::user()->organisasi->public_id ? 'readonly' : ''}}>
                                                @if($errors->has('nama'))
                                                    <label id="basic-error" class="validation-invalid-label"
                                                           for="basic">
                                                        {{$errors->first('nama')}}
                                                    </label>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Singkatan Unit Kerja / Satker</label>
                                                <input type="text" name="singkatan" id="singkatan" class="form-control"
                                                       value="{{$data->singkatan}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Kode</label>
                                                <input type="text" name="kode" class="form-control"
                                                       value="{{$data->nomenklatur_path == null ? $data->nomenklatur_ori : $data->nomenklatur_path}}"
                                                    {{$data->id == \App\Services\Auth::user()->organisasi->public_id ? 'readonly' : ''}}>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="d-block">Status</label>
                                                <div class="form-check form-check-inline mt-1">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input-styled"
                                                               name="status"
                                                               {{$data->id == \App\Services\Auth::user()->organisasi->public_id ? 'disabled' : ''}}
                                                               value="AKTIF"
                                                               {{ $data->status == 'AKTIF' ? 'checked' : '' }}
                                                               data-fouc>
                                                        <span class="badge badge-success">AKTIF</span>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input-styled"
                                                               name="status"
                                                               {{$data->id == \App\Services\Auth::user()->organisasi->public_id ? 'disabled' : ''}}
                                                               value="NON-AKTIF"
                                                               {{ $data->status == 'NON-AKTIF' ? 'checked' : '' }}
                                                               data-fouc>
                                                        <span class="badge badge-secondary">NON-AKTIF</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat Instansi</label>
                                                <textarea name="alamat" id="alamat"
                                                          class="form-control">{{$data->alamat}}</textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Long </label>
                                                        <input type="text" name="long" id="long" class="form-control"
                                                               placeholder="Masukkan longtitude..."
                                                               value="{{ $data->long }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Lat</label>
                                                        <input type="text" name="lat" id="lat" class="form-control"
                                                               placeholder="Masukkan latitude..."
                                                               value="{{ $data->lat }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary btn-labeled btn-labeled-left"><b><i
                                                class="icon-floppy-disk"></i></b>Perbarui
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endpush

@section('footer-script')
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endsection
