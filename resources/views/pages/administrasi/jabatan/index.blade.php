@extends('layouts.begin_back')

@section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('Jabatan', 'active')

@section('title', 'Jabatan')

@section('content')

    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-folder-check mr-2"></i> <span class="font-weight-semibold">Jabatan</span> - List</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="{{route('jabatan.create')}}"
               class="btn bg-teal btn-labeled btn-labeled-left">
                <b><i class="icon-plus2"></i></b> Buat baru
            </a>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header bg-white">
                                <h6 class="card-title">Jabatan</h6>
                            </div>
                            <table class="table datatable-responsive-control-right table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 30%">Unit Kerja</th>
                                    <th style="width: 60%">Jabatan</th>
                                    <th style="width: 60%">Status</th>
                                    <th style="width: 20%" class="text-center">Aksi</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->organisasi->nama }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td class="text-center">
                                            @if($item->status == 'AKTIF')
                                                <span class="badge badge-success">AKTIF</span>
                                            @else
                                                <span class="badge badge-secondary">{{ $item->status }}</span>
                                            @endif
                                        </td>
                                        <td style="white-space: nowrap">
                                            <a href="{{ route('jabatan.edit', ['id' => $item->id]) }}"
                                               class="list-icons-item text-info btn-icon">
                                                <i class="icon-pencil"></i>
                                            </a>
                                            <a href="#" data-id="{{$item->id}}"
                                               class="list-icons-item text-danger btn-icon delete-organisasi">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endpush

@section('footer-script')
    <script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.datatable-responsive-control-right').on('click', '.delete-organisasi', function (e) {
                var id = $(this).data('id');

                swal.fire({
                    title: 'Hapus Jabatan/ Satker',
                    text: 'Apakah Anda yakin ingin menghapus Jabatan / Satker ini ?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal!',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-link',
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            method: 'POST',
                            dataType: 'json',
                            url: "{{ route('jabatan.destroy') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id": id,
                            }
                        }).done(function (response) {
                            if (response.status == true) {
                                swal.fire({
                                    title: 'Yeay!',
                                    text: response.message,
                                    type: 'success'
                                });
                                location.reload();
                            } else if (response.status == false) {
                                swal.fire({
                                    title: 'Auch!',
                                    text: response.message,
                                    type: 'error'
                                });
                                location.reload();
                            }
                        })
                    }
                });
            });

            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                dropdownAutoWidth: true,
                width: 'auto'
            });
        });

        function initDatatable() {
            $('.datatable-responsive-control-right').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('jabatan.get') }}",
                columns: [
                    {
                        "data": "organisasi_id"
                    },
                    {
                        "data": "nama"
                    },
                    {
                        "data": "status"
                    },
                    {
                        // "data": 'action',
                        "name": 'action',
                        "orderable": false,
                        "searchable": false
                    },
                    {
                        "data": null,
                        "defaultContent": ""
                    },
                ],
                responsive: {
                    details: {
                        type: 'column',
                        target: -1
                    }
                },
                columnDefs: [
                    {
                        "className": "dt-body-center",
                        "targets": [3],
                        "render": function (data, type, row) {
                            let id = row['id'];
                            return '<a href="/jabatan/' + id + '/edit" class="list-icons-item text-info btn-icon">' +
                                '   <i class="icon-pencil"></i>' +
                                '</a>' +
                                '   <a href="#" data-id="' + id + '" class="list-icons-item text-danger btn-icon delete-organisasi">' +
                                '   <i class="icon-trash"></i>' +
                                '</a>';
                        }
                    },
                    {
                        className: 'control',
                        orderable: false,
                        targets: -1
                    },
                    {
                        width: "100px",
                    },
                    {
                        orderable: false,
                    }
                ]
            });
        }
    </script>
@endsection

