@extends('layouts.begin_back')

@section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('pengguna', 'active')

@section('title', 'Pengguna Baru')

@section('content')
<!-- Page header -->
<div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex">
        <h4><i class="icon-users mr-2"></i> <span class="font-weight-semibold">Pengguna</span> - Baru</h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>

    <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
        <a href="{{ route('pengguna.index') }}" class="btn bg-grey btn-labeled btn-labeled-left"><b><i
                    class="icon-arrow-left8"></i></b>Kembali</a>
    </div>
</div>
<!-- /page header -->


<div class="page-content pt-0">
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title"> Form Buat Pengguna </h5>
                        </div>
                        <form action="{{ route('pengguna.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nomor_induk">Nomor Induk Pegawai <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" name="nomor_induk" class="form-control"
                                                placeholder="Masukkan Nomor Induk Pegawai.."
                                                value="{{ old('nomor_induk') }}">
                                            @error('nomor_induk')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="NIK">NIK <span class="text-danger">*</span></label>
                                            <input type="text" name="nik" class="form-control"
                                                placeholder="Masukkan NIK.." value="{{ old('nik') }}">
                                            @error('nik')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="Nama">Nama Lengkap <span class="text-danger">*</span></label>
                                            <input type="text" name="nama" class="form-control"
                                                placeholder="Masukkan Nama..." value="{{ old('nama') }}">
                                            @error('nama')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="telepon">Nomor Seluler <span 
                                                class="text-danger">*</span></label></label>
                                            
                                            <input type="text" name="telepon" class="form-control"
                                                placeholder="Masukkan Nomor Seluler..."
                                                value="{{ old('telepon') }}">
                                            <span class="text-muted form-text">Disarankan nomor yang digunakan
                                                <i><b>Whatsapp</b></i></span>
                                            @error('telepon')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Nama Pengguna <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Masukkan Nama Pengguna..." value="{{ old('username') }}">
                                            @error('username')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email <span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control"
                                                placeholder="Masukkan Email..." value="{{ old('email') }}"
                                                autocomplete="email">
                                            @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Kata Sandi <span class="text-danger">*</span></label>
                                            <input type="password" name="password" class="form-control"
                                                placeholder="Minimal 8 karakter..." value="{{ old('password') }}"
                                                autocomplete="new-password">
                                            @error('password')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation">Konfirmasi Kata Sandi <span
                                                    class="text-danger">*</span></label>
                                            <input type="password" name="password_confirmation" class="form-control"
                                                placeholder="Ketik ulang kata sandi..."
                                                value="{{ old('password_confirmation') }}" autocomplete="new-password">
                                            @error('password_confirmation')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="roles">Hak Akses <span class="text-danger">*</span></label>
                                            <select name="roles" class="form-control select2" id="roles"
                                                onchange="selectedRoles()" data-placeholder="Pilih Hak Akses..."
                                                style="width:100%;">
                                                <option></option>
                                                {{-- @foreach($roles as $item)
                                                <option value="{{ $item->name }}">
                                                    {{ $item->name }}</option>
                                                @endforeach --}}
                                            </select>
                                            @error('roles')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        {{-- <div class="form-group" id="check-role">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="YES" name="send_outside"
                                                    {{ old('send_outside') == "YES" ? 'checked' : '' }}
                                                    class="custom-control-input" id="custom-check-box">
                                                <label class="custom-control-label" for="custom-check-box">
                                                    <div id="check-user">
                                                        <b>User</b> ini dapat mengirim dan atau menerima Surat dari Luar
                                                        Instansi / Unit Kerja / Satuan Kerja?
                                                    </div>
                                                    <div id="check-pencatat-surat">
                                                        Pengguna ini menjadi <b>Pencatat Surat Utama</b>?
                                                    </div>
                                                    <div id="check-unit-kearsipan">
                                                        Pengguna ini menjadi <b>Pengelola retensi diatas 10 tahun</b>?
                                                    </div>
                                                </label>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="organisasi_id">Instansi / Unit Kerja <span
                                                    class="text-danger">*</span></label>
                                            <select name="organisasi_id" id="organisasi_id" class="form-control select2"
                                                data-placeholder="Pilih Instansi / Unit Kerja..." style="width:100%;">
                                                <option></option>
                                                @foreach($organisasi as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->nama }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('organisasi_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="jabatan_id">Jabatan <span class="text-danger">*</span></label>
                                            <select name="jabatan_id" id="jabatan_id" class="form-control select2"
                                                data-placeholder="Pilih Jabatan..." style="width:100%;">
                                                <option></option>
                                                @foreach($jabatan as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->nama }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('jabatan_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="grup_jabatan_id">Grup Jabatan</label>
                                            <select name="grup_jabatan_id" id="grup_jabatan_id" class="form-control select2"
                                                data-placeholder="Pilih Grup Jabatan..." style="width:100%;">
                                                <option></option>
                                                {{-- @foreach($grupJabatan as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ old('grup_jabatan_id') == $item->id ? 'selected' : '' }}>
                                                    {{ $item->nama }}
                                                </option>
                                                @endforeach --}}
                                            </select>
                                            @error('grup_jabatan_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="tipe_pengguna_id">Jenis Pengguna</label>
                                            <select name="tipe_pengguna_id" id="tipe_pengguna_id" class="form-control select2"
                                                data-placeholder="Pilih Jenis Pengguna..." style="width:100%;">
                                                <option></option>
                                                {{-- @foreach($tipePengguna as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ old('tipe_pengguna_id') == $item->id ? 'selected' : '' }}>
                                                    {{ $item->nama }}
                                                </option>
                                                @endforeach --}}
                                            </select>
                                            @error('tipe_pengguna_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Foto</label>
                                            <div class="media mt-0">
                                                <div class="mr-3">
                                                    <a href="#">
                                                        <img src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}"
                                                            width="60" height="60" class="rounded-round" alt=""
                                                            id="previewAvatar"
                                                            style="object-fit: cover; object-position: 50% 0%;">
                                                    </a>
                                                </div>

                                                <div class="media-body">
                                                    <input type="file" name="file" class="form-input-styled" id="avatar"
                                                        data-fouc>
                                                    <span class="form-text text-muted">Format yang didukung: .jpg .jpeg
                                                        .png</span>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group" id="check-notifikasi">
                                            <label for="notification">Notifikasi</label>
                                            <div class="form-check form-check-switchery">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input-switchery" name="notif_wa" value="true" {{ old('notif_wa') == true ? 'checked':'' }} data-fouc>
                                                    Terima notifikasi <i><b>Whatsapp</b></i> dari SRIKANDI
                                                </label>
                                            </div>
                                            <div class="form-check form-check-switchery">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input-switchery" name="notif_email" value="true" {{ old('notif_email') == true ? 'checked':'' }} data-fouc>
                                                    Terima notifikasi <i><b>Email</b></i> dari SRIKANDI
                                                </label>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                                            class="icon-floppy-disk"></i></b> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addon-script')
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endpush

@section('footer-script')
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
<script>
    $('#check-role').hide();
    $("#check-notifikasi").hide();

    function selectedRoles() {
        var type = $("#roles").val();

        if (type == 'User') {
            $('#check-role').show();
            $("#check-user").show();
            $("#check-notifikasi").show();

            $("#check-pencatat-surat").hide();
            $("#check-unit-kearsipan").hide();
        } else if (type == 'Tata Usaha / Sekretaris') {
            $('#check-role').show();
            $("#check-pencatat-surat").show();

            $("#check-unit-kearsipan").hide();
            $("#check-user").hide();
            $("#check-notifikasi").hide();
        } else if (type == 'Unit Kearsipan') {
            $('#check-role').show();
            $("#check-unit-kearsipan").show();

            $("#check-pencatat-surat").hide();
            $("#check-user").hide();
            $("#check-notifikasi").hide();
        } else {
            $('#check-role').hide();
            $("#check-user").hide();
            $("#check-notifikasi").hide();
            $("#check-unit-kearsipan").hide();
            $("#check-pencatat-surat").hide();
        }
    }

    $(document).ready(function () {
        $('.select2').select2({
            allowClear: true
        });

        //preview avatar
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewAvatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatar").change(function () {
            readURL(this);
        });

        // $('#organisasi_id').select2({
        //     allowClear:true,
        
        //     ajax:{
        //         url: "http://srikandi.layanan.go.id/api/unit-kerja",
        //         type: 'GET',
        //         dataType: 'json',
        //         delay:250,
        //         data: function(params){
        //             return{
        //                 search:params.term,
        //                 page:params.page || 1
        //             };
        //         },
        //         processResults: function(response){
        //             return{
        //                 results:response
        //             };
        //         },
        //         cache: true
        //     },
        // });
    });

</script>
@endsection