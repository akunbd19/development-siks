
 @extends('layouts.begin_back')
 
 @section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('Group Jabatan', 'active')

@section('title', 'Group Jabatan')

@section('content')

<!-- Page header -->
<div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex">
        <h4><i class="icon-folder-check mr-2"></i> <span class="font-weight-semibold">Group Jabatan</span> - List</h4>
        {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
    </div>

    {{-- <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
        <button type="button" data-toggle="modal" data-target="#create-modal-tipe-pengguna" class="btn bg-indigo btn-labeled btn-labeled-left"><b><i class="icon-plus2"></i></b>Buat baru</button>
    </div> --}}
</div>
<!-- /page header -->


<div class="page-content pt-0">
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header bg-white">
                            <h6 class="card-title">Group Jabatan</h6>
                        </div>
                        <table class="table datatable-responsive-control-right table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Kode Klasifikasi</th>
                                    <th>Berkas</th>
                                    <th>Kurun Waktu</th>
                                    <th>Jumlah Item</th>
                                    <th>Akhir Retensi Inaktif</th>
                                    <th width="5%" class="text-center">Aksi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>IK.02</td>
                                    <td>1 - ANRI</td>
                                    <td>2 September 2021 s/d 2 September 2021</td>
                                    <td>1</td>
                                    <td>7 September 2026</td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <a href="{{ route('arsip.show',['id' => 6]) }}" class="list-icons-item text-secondary"><i
                                                    class="icon-eye"></i></a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addon-script')
<script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endpush

@section('footer-script')
<script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
@endsection