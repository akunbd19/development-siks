<!-- Header with logos -->
<div class="navbar-header navbar-dark bg-teal-800 d-none d-md-flex align-items-md-center">
    <div class="navbar-brand navbar-brand-md bg-teal-800">
        <img src="{{ asset('global_assets/images/logo_light.png') }}">
        {{-- <a href="{{ route('dashboard.index') }}" class="d-inline-block">
        </a> --}}
    </div>

    <div class="navbar-brand navbar-brand-xs">
        <a href="{{ route('dashboard.index') }}" class="d-inline-block">
            <img src="{{ asset('global_assets/images/logo_light.png') }}">
        </a>
    </div>
</div>
<!-- /header with logos -->

<!-- Mobile controls -->
<div class="d-flex flex-1 d-md-none">
    <div class="navbar-brand mr-auto">
        <a href="{{ route('dashboard.index') }}" class="d-inline-block">
            <img src="{{ asset('global_assets/images/logo_light.png') }}" alt="">
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
        <i class="icon-tree5"></i>
    </button>

    <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
        <i class="icon-paragraph-justify3"></i>
    </button>
</div>
<!-- /mobile controls -->

<!-- Navbar content -->
<div class="collapse navbar-collapse" id="navbar-mobile">
    <ul class="navbar-nav mr-md-auto">
        <li class="nav-item">
            <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                <i class="icon-paragraph-justify3"></i>
            </a>
        </li>
    </ul>
    <span class="navbar-text d-block d-xl-inline-block">
        <i class="icon-user-check mr-2"></i>
        Hak Akses: {{\App\Services\Auth::user()->roles[0]->name}}
    </span>

    <ul class="navbar-nav">

        <li class="nav-item dropdown dropdown-user">
            <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle mr-2"
                     height="34" alt="">
                {{--                <span>{{ \App\Services\Auth::user()->nama }}</span>--}}
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Pengaturan Akun</a>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="dropdown-item"><i class="icon-switch2"></i> Keluar</a>


                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</div>
<!-- /navbar content -->
