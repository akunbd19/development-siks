<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }} - @yield('title')</title>

    {{-- <!-- Meta Favico -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('favicon.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon.ico/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- SEO Tag -->
    <meta name="title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta name="description"
        content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://srikandi.layanan.go.id/">
    <meta property="og:title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta property="og:description"
        content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">
    <meta property="og:image" content="{{ asset('images/seo_image') }}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="http://srikandi.layanan.go.id/">
    <meta property="twitter:title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta property="twitter:description"
        content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">
    <meta property="twitter:image" content="{{ asset('images/seo_image') }}"> --}}

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
</head>

<body>

    <!-- Page content -->
    @yield('content')
    <!-- /page content -->
    			<!-- Footer -->
			{{--<div class="navbar navbar-expand-lg navbar-light">
				--}}{{--<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>--}}{{--

				<div class="navbar-collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; {{ date('Y') }}. <a href="https://www.anri.go.id/" target="_blank">Arsip Nasional Republik
                            Indonesia</a>
                    </span>

					<ul class="navbar-nav ml-lg-auto">
						--}}{{-- <li class="nav-item"><a href="https://arsip.go.id/" class="navbar-nav-link" target="_blank"><i class="icon-link2 mr-2"></i> SRIKANDI Versi 1</a></li> --}}{{--
						--}}{{-- <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li> --}}{{--
						--}}{{-- <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li> --}}{{--
					</ul>
				</div>
			</div>--}}
			<!-- /footer -->

    <!-- Core JS files -->
    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <!-- /theme JS files -->

</body>

</html>
