<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }} - @yield('title')</title>

    <!-- Meta Favico -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('favicon.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.ico/favicon-16x16.png') }}">
    {{--    <link rel="manifest" href="{{ asset('favicon.ico/manifest.json') }}">--}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- SEO Tag -->
    <meta name="title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta name="description"
          content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://srikandi.layanan.go.id/">
    <meta property="og:title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta property="og:description"
          content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">
    <meta property="og:image" content="{{ asset('images/seo_image') }}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="http://srikandi.layanan.go.id/">
    <meta property="twitter:title" content="Srikandi - Sistem Informasi Kearsipan Dinamis Terintegrasi">
    <meta property="twitter:description"
          content="Srikandi ditetapkan agar setiap lingkungan Kementerian/Lembaga dapat menggunakan aplikasi umum dalam pengelolaan arsip dinamis di lingkungan instansi masing-masing">
    <meta property="twitter:image" content="{{ asset('images/seo_image') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    @yield('styles')
</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-teal-800">
    @include('include.navbar')
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-light sidebar-main sidebar-expand-md sidebar-fullscreen">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media d-block">
                        <div class="mb-3 text-center">
                            <a href="#">
                                <img src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}"
                                             width="40" height="40"
                                             class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body text-center">
                            <div class="media-title font-weight-semibold">{{\App\Services\Auth::user()->nama}}</div>
                            <div class="font-size-xs opacity-50">
                                <p class="mb-0">{{\App\Services\Auth::user()->jabatan->nama}}</p>
                                <p class="mb-0">{{\App\Services\Auth::user()->organisasi->singkatan}}</p>
                                <p class="mb-0">{{\App\Services\Auth::user()->organisasi_public->singkatan}}</p>
                            </div>
                        </div>

                        <div class="ml-3 align-self-center">
                            {{-- <a href="#" class="text-white"><i class="icon-cog3"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                @include('include.menu')
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

    @yield('content')


    <!-- Footer -->
    @include('include.footer')
    <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

{{-- @include('messages.development') --}}

<!-- Core JS files -->
<script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
<script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{{ asset('global_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/notifications/noty.min.js') }}"></script>
@stack('addon-script')

<script src="{{ asset('assets/js/app.js') }}"></script>
@yield('footer-script')
<!-- /theme JS files -->
</body>

</html>
